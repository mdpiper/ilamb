The ILAMB Benchmarking System
=============================

The International Land Model Benchmarking (ILAMB) project is a
model-data intercomparison and integration project designed to improve
the performance of land models and, in parallel, improve the design of
new measurement campaigns to reduce uncertainties associated with key
land surface processes. Building upon past model evaluation studies,
the goals of ILAMB are to:

* develop internationally accepted benchmarks for land model
  performance, promote the use of these benchmarks by the
  international community for model intercomparison,
* strengthen linkages between experimental, remote sensing, and
  climate modeling communities in the design of new model tests and
  new measurement programs, and
* support the design and development of a new, open source,
  benchmarking software system for use by the international community.

It is the last of these goals to which this repository is
concerned. We have developed a python-based generic benchmarking
system, initially focused on assessing land model performance.

Useful Information
------------------

* `Documentation
  <http://climate.ornl.gov/~ncf/ILAMB/docs/index.html>`_ of the public
  API is included in the repository, but also hosted if you follow the
  link.
* `Sample output
  <http://www.climatemodeling.org/~nate/ILAMB/index.html>`_ gives you
  an idea of the scope and magnitude of the package capabilities.
* You may cite the software package by using the following reference (DOI:10.18139/ILAMB.v002.00/1251621).

Funding
-------

This research was performed for the Biogeochemistry--Climate Feedbacks
Scientific Focus Area, which is sponsored by the Regional and Global
Climate Modeling (RGCM) Program in the Climate and Environmental
Sciences Division (CESD) of the Biological and Environmental Research
(BER) Program in the U.S. Department of Energy Office of Science.
